// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import portAudio from "naudiodon";
import fs from "fs";
import ffmpeg from "fluent-ffmpeg";
import wav from "wav";

const channelCount = 2;
const sampleRate = 44100;
const sampleFormat = 16;

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse
) {
    res.writeHead(200, {
        "Content-Type": `audio/flac`,
        "Transfer-Encoding": "chunked",
    });

    const ai = portAudio.AudioIO({
        inOptions: {
            channelCount,
            deviceId: -1,
            closeOnError: false,
            sampleFormat,
            sampleRate,
        },
    });

    const writer = new wav.Writer({
        channels: channelCount,
        sampleRate,
        bitDepth: sampleFormat,
    });

    ai.start();

    ai.pipe(writer);

    const converted = ffmpeg().input(writer).toFormat("flac");

    converted.pipe(res);

    // const f = fs.createWriteStream(
    //     `/tmp/${new Date().getUTCMilliseconds()}.flac`
    // );
    // flac.pipe(f);
}
