import type { NextPage } from "next";
import { SyntheticEvent, useRef } from "react";

const Home: NextPage = () => {
    const audioRef = useRef<null | HTMLAudioElement>(null);
    const handlePlay = (e: SyntheticEvent<HTMLButtonElement>) => {
        if (!audioRef.current) return;
        audioRef.current.muted = false;
        audioRef.current.play();
    };

    return (
        <div>
            <p>Streams Audio</p>
            <audio
                ref={audioRef}
                onLoad={(e) => console.log(e)}
                onError={(e) => console.log(e)}
                controls
                src="/api/audio"
            />
            <button onClick={handlePlay}>Play</button>
        </div>
    );
};

export default Home;
